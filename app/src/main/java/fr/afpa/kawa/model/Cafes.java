package fr.afpa.kawa.model;

import java.io.Serializable;
import java.util.List;

public class Cafes implements Serializable {

    private List<Records> records;

    public class Records {
        private Fields fields;

        public Fields getFields() {
            return fields;
        }
    }

    public class Fields {
        private String arrondissement;
        private String adresse;
        private String prix_salle;
        private String nom_du_cafe;
        private String prix_terasse;
        private String prix_comptoir;
        private double[] geoloc;

        public String getArrondissement() {
            return arrondissement;
        }

        public String getAdresse() {
            return adresse;
        }

        public String getPrix_salle() {
            return prix_salle;
        }

        public String getNom_du_cafe() {
            return nom_du_cafe;
        }

        public String getPrix_terasse() {
            return prix_terasse;
        }

        public String getPrix_comptoir() {
            return prix_comptoir;
        }

        public double[] getGeoloc() {
            return geoloc;
        }
    }

    public List<Records> getRecords() {
        return records;
    }
}
