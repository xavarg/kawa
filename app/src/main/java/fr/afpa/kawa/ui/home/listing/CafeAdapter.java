package fr.afpa.kawa.ui.home.listing;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import fr.afpa.kawa.R;
import fr.afpa.kawa.model.Cafes;

public class CafeAdapter extends ArrayAdapter<Cafes.Records> {

    private int resId;

    public CafeAdapter(Context context, int resource, List<Cafes.Records> objects) {
        super(context, resource, objects);

        resId = resource; // R.layout.item_forecast
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // chargement du layout
        convertView = LayoutInflater.from(getContext()).inflate(resId, null);

        // récupération des Views
        TextView coffee_name = convertView.findViewById(R.id.coffee_name);
        TextView coffee_zipcode = convertView.findViewById(R.id.coffee_zipcode);


        // récupération de l'objet ForecastList
        Cafes.Records item = getItem(position);

        // affichage ddes informations
        coffee_name.setText(item.getFields().getNom_du_cafe());
        coffee_zipcode.setText(item.getFields().getArrondissement());


        return convertView;
    }
}


