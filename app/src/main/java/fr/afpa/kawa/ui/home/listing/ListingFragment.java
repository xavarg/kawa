package fr.afpa.kawa.ui.home.listing;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import fr.afpa.kawa.R;
import fr.afpa.kawa.model.Cafes;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListingFragment extends Fragment {
    private static final String PARAM_OBJECT_COFFEE = "PARAM_OBJECT_COFFEE";
    private Cafes myCoffees;
    private ListView listViewCafes;


    public ListingFragment() {
        // Required empty public constructor
    }

    public static ListingFragment newInstance(Cafes myCoffees) {
        ListingFragment fragment = new ListingFragment();

        Bundle myBundle = new Bundle();
        myBundle.putSerializable(PARAM_OBJECT_COFFEE, myCoffees);
        fragment.setArguments(myBundle);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            myCoffees = (Cafes) getArguments().get(PARAM_OBJECT_COFFEE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View viewFragment = inflater.inflate(R.layout.fragment_listing, container, false);

        listViewCafes = viewFragment.findViewById(R.id.listViewCafes);
        listViewCafes.setAdapter(new CafeAdapter(
                getContext(),
                R.layout.item_listing,
                myCoffees.getRecords()
        ));
        return viewFragment;
    }

}
