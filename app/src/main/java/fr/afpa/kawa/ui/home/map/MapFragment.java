package fr.afpa.kawa.ui.home.map;

import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mapbox.geojson.Feature;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.style.layers.PropertyFactory;
import com.mapbox.mapboxsdk.style.layers.SymbolLayer;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;

import java.util.List;

import fr.afpa.kawa.R;
import fr.afpa.kawa.model.Cafes;

/**
 * A simple {@link Fragment} subclass.
 */
public class MapFragment extends Fragment {
    private static final String PARAM_OBJECT_COFFEE = "PARAM_OBJECT_COFFEE";

    private Cafes myCoffees;
    private MapView mapView;

    public MapFragment() {
        // Required empty public constructor
    }

    public static MapFragment newInstance(Cafes myCoffees) {
        MapFragment fragment = new MapFragment();

        Bundle myBundle = new Bundle();
        myBundle.putSerializable(PARAM_OBJECT_COFFEE, myCoffees);
        fragment.setArguments(myBundle);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            myCoffees = (Cafes) getArguments().get(PARAM_OBJECT_COFFEE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // carte MapBox
        Mapbox.getInstance(getContext(), "pk.eyJ1IjoiYWZwYWN5YmVyIiwiYSI6ImNqc3Z2OW9ydjAyaDk0NG95eDRvNTU2M3UifQ.ahmYOsJPYsltMh37af_Gvw");

        // Inflate the layout for this fragment
        View fragmentView = inflater.inflate(R.layout.fragment_map, container, false);

        mapView = fragmentView.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(@NonNull MapboxMap mapboxMap) {
                mapboxMap.setStyle(Style.MAPBOX_STREETS, new Style.OnStyleLoaded() {
                    @Override
                    public void onStyleLoaded(@NonNull Style style) {
                        // Map is set up and the style has loaded. Now you can add data or make other map adjustments

                        List<Cafes.Records> records = myCoffees.getRecords();

                        for (int i = 0; i < records.size(); i++) {
                            Cafes.Fields fields = records.get(i).getFields();

                            // Add the marker image to map
                            style.addImage("marker-icon-id" + i,
                                    BitmapFactory.decodeResource(
                                            getResources(), R.drawable.mapbox_marker_icon_default));

                            GeoJsonSource geoJsonSource = new GeoJsonSource("source-id" + i, Feature.fromGeometry(
                                    Point.fromLngLat(fields.getGeoloc()[1], fields.getGeoloc()[0])));
                            style.addSource(geoJsonSource);

                            SymbolLayer symbolLayer = new SymbolLayer("layer-id" + i, "source-id" + i);
                            symbolLayer.withProperties(
                                    PropertyFactory.iconImage("marker-icon-id" + i)
                            );
                            style.addLayer(symbolLayer);
                        }
                    }
                });
            }
        });

        return fragmentView;
    }


    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mapView.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }
}
